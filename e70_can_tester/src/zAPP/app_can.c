/*
 * app_can.c
 *
 * Created: 4/9/2019 5:54:24 PM
 *  Author: lphillips_admin
 */ 


#include "app_can.h"


static struct mcan_module mcan_instance;

static uint8_t tx_message_0[CONF_MCAN_ELEMENT_DATA_SIZE];
static uint8_t tx_message_1[CONF_MCAN_ELEMENT_DATA_SIZE];


void app_can_config (void)
{
	uint32_t i;
	
	for (i = 0; i < CONF_MCAN_ELEMENT_DATA_SIZE; i++) {
		tx_message_0[i] = i;
		tx_message_1[i] = i + 0x80;
	}
	
}


void app_can_config (void)
{
	
	/* Initialize the module. */
	struct mcan_config config_mcan;
	mcan_get_config_defaults(&config_mcan);
	mcan_init(&mcan_instance, MCAN_MODULE, &config_mcan);
	
	mcan_start(&mcan_instance);
	
	/* Enable interrupts for this MCAN module */
	irq_register_handler(MCAN1_INT0_IRQn, 1);
	mcan_enable_interrupt(&mcan_instance, MCAN_FORMAT_ERROR | MCAN_ACKNOWLEDGE_ERROR | MCAN_BUS_OFF);
}



#define CAN_RX_FILTER_ID_HI  0x200
#define CAN_RX_FILTER_ID_LO  0x100
#define MCAN_RX_STANDARD_FILTER_INDEX_0					0

void app_can_set_filter_1 (void)
{
	struct mcan_standard_message_filter_element sd_filter;
	
	mcan_get_standard_message_filter_element_default(&sd_filter);
	sd_filter.S0.bit.SFT    = 0 ;   /*Range filter from SF1ID to SF2ID (SF2ID ? SF1ID)*/
	sd_filter.S0.bit.SFID2 = CAN_RX_FILTER_ID_HI;  /*high range standard address*/
	sd_filter.S0.bit.SFID1 = CAN_RX_FILTER_ID_LO; /*low range standard address*/
	sd_filter.S0.bit.SFEC =  MCAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_STRXBUF_Val;     /*Store in Rx FIFO 0 if filter matches*/

	mcan_set_rx_standard_filter(&mcan_instance, &sd_filter, MCAN_RX_STANDARD_FILTER_INDEX_0);
	mcan_enable_interrupt(&mcan_instance, MCAN_RX_FIFO_0_NEW_MESSAGE);
}